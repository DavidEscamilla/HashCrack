/*  This file is part of HASHCRACK.

	Foobar is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.


	Foobar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Foobar.  If not, see <https://www.gnu.org/licenses/>.

*/

package GUI;

import java.awt.Desktop;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;

import Clases.Shell;
import Config.Configuration;

public class Interfaz extends javax.swing.JFrame {

	public Image getIconImage() { // metodo para a�adir imagen al icono de la app.

		Image retValue = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("resources/icon.png"));

		return retValue;

	}

	public Interfaz(String usuario, String password, String server) {

		initComponents(usuario, password, server);

	}

	public void openWebpage(String urlString) { // metodo para abrir pagina web en navegador.

		try {

			Desktop.getDesktop().browse(new URL(urlString).toURI());

		} catch (Exception e) {

			System.out.println(e.getMessage());

		}
	}

	@SuppressWarnings("unchecked")
	private void initComponents(String usuario, String pass, String server) {

		jScrollPane1 = new javax.swing.JScrollPane();
		jTextArea1 = new javax.swing.JTextArea();
		jTextField1 = new javax.swing.JTextField();
		jButton1 = new javax.swing.JButton();
		jTextField2 = new javax.swing.JTextField();
		jTextField3 = new javax.swing.JTextField();
		jOptionPane1 = new javax.swing.JOptionPane();
		jButton2 = new javax.swing.JButton();
		jLabel1 = new javax.swing.JLabel();
		jLabel2 = new javax.swing.JLabel();
		jLabel5 = new javax.swing.JLabel();
		jSeparator1 = new javax.swing.JSeparator();
		jLabel3 = new javax.swing.JLabel();

		// menu

		jMenuBar1 = new javax.swing.JMenuBar();
		jMenu1 = new javax.swing.JMenu();
		jMenu2 = new javax.swing.JMenu();
		jMenu3 = new javax.swing.JMenu();
		jMenu4 = new javax.swing.JMenu();
		jMenuItem1 = new javax.swing.JMenuItem();
		jMenuItem2 = new javax.swing.JMenuItem();
		jMenuItem3 = new javax.swing.JMenuItem();
		jMenuItem4 = new javax.swing.JMenuItem();
		jMenuItem5 = new javax.swing.JMenuItem();
		jMenuItem6 = new javax.swing.JMenuItem();
		jMenuItem7 = new javax.swing.JMenuItem();
		jMenuItem8 = new javax.swing.JMenuItem();
		jComboBox1 = new javax.swing.JComboBox<>();
		jComboBox2 = new javax.swing.JComboBox<>();
		

		// end of initialize variables

		try {

			Configuration conf = new Configuration();
			conf.checkColor();

		} catch (IOException e2) {

			e2.printStackTrace();
		}

		setTitle("Happy hunting ");
		setIconImage(getIconImage());
		setBackground(new java.awt.Color(0, 204, 0));
		setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
		setResizable(false);
		setFont(new java.awt.Font("Ebrima", 0, 12)); // NOI18N

		jSeparator1.setForeground(new java.awt.Color(0, 0, 0));

		jTextArea1.setFont(new java.awt.Font("Ebrima", 0, 15)); // NOI18N
		jTextArea1.setColumns(20);
		jTextArea1.setRows(5);
		jTextArea1.setText("Choose and option & happy hunting ;)....");
		jTextArea1.setEditable(false);

		jScrollPane1.setViewportView(jTextArea1);

		jLabel3.setFont(new java.awt.Font("Ebrima", 0, 12)); // NOI18N
		jLabel3.setText("Option:");

		jComboBox1.setFont(new java.awt.Font("Ebrima", 0, 12));
		jTextField1.setFont(new java.awt.Font("Ebrima", 0, 12));
		jTextField1.setToolTipText("");
		jTextField2.setFont(new java.awt.Font("Ebrima", 0, 12));
		jTextField3.setFont(new java.awt.Font("Ebrima", 0, 12));

		jLabel5.setFont(new java.awt.Font("DialogInput", 0, 12)); // NOI18N
		jLabel5.setVisible(true);

		jButton1.setText(">");
		jButton1.addActionListener(new java.awt.event.ActionListener() {
			@SuppressWarnings("static-access")
			public void actionPerformed(java.awt.event.ActionEvent evt) {

				jButton1ActionPerformed(evt);

				// recuperacion de los valores

				String opcion = (String) jComboBox1.getSelectedItem();
				String hash = jTextField2.getText().toString();
				String name = jTextField3.getText().toString();
				String extension = jComboBox2.getSelectedItem().toString();
				String user_val = usuario;
				String user_pass = pass;
				String user_ser = server;

				System.out.println("Usuario xfavor: " + usuario);

				// condicional para comprobar

				if (opcion.equals("Crack!") && name.equals("")) {

					jOptionPane1.showMessageDialog(rootPane, "Introduzca el nombre", "Advertencia", 2);

				} else if (opcion.equals("PotFile") && name.equals("")) {

					jOptionPane1.showMessageDialog(rootPane, "Introduzca el nombre", "Advertencia", 2);

				} else if (opcion.equals("View") && name.equals("")) {

					jOptionPane1.showMessageDialog(rootPane, "Introduzca el nombre", "Advertencia", 2);

				} else if (opcion.equals("Convert") && name.equals("") && hash.equals("")) {

					jOptionPane1.showMessageDialog(rootPane, "Introduzca palabra y nombre", "Advertencia", 2);

				} else if (opcion.equals("Convert") && hash.equals("")) {

					jOptionPane1.showMessageDialog(rootPane, "Introduzca la palabra", "Advertencia", 2);

				} else if (opcion.equals("Convert") && name.equals("")) {

					jOptionPane1.showMessageDialog(rootPane, "Introduzca el nombre", "Advertencia", 2);

				} else if (opcion.equals("Upload") && name.equals("")) {

					jOptionPane1.showMessageDialog(rootPane, "Introduzca el nombre", "Advertencia", 2);

				} else {

					Shell shell_con = new Shell();

					jTextArea1.setText(shell_con.Consola(opcion, hash, name, extension, user_val, user_pass, user_ser));

				}

			}
		});

		jTextField2.setBackground(new java.awt.Color(204, 204, 204));
		jTextField2.setFont(new java.awt.Font("Ebrima", 0, 12)); // NOI18N
		jTextField2.setSelectionColor(new java.awt.Color(153, 51, 0));
		jTextField3.setBackground(new java.awt.Color(204, 204, 204));
		jTextField3.setFont(new java.awt.Font("Ebrima", 0, 12)); // NOI18N

		jTextField2.addActionListener(new java.awt.event.ActionListener() {

			public void actionPerformed(java.awt.event.ActionEvent evt) {

				jTextField2ActionPerformed(evt);

			}
		});

		jLabel1.setText("Name:");
		jLabel2.setText(" h4$h:");
		jButton2.setFont(new java.awt.Font("Ebrima", 0, 12)); // NOI18N

		jLabel1.setFont(new java.awt.Font("Ebrima", 0, 12)); // NOI18N
		jLabel2.setFont(new java.awt.Font("Ebrima", 0, 12)); // NOI18N
		jMenu1.setFont(new java.awt.Font("Ebrima", 0, 12)); // NOI18N
		jMenuItem2.setFont(new java.awt.Font("Ebrima", 0, 11)); // NOI18N
		jMenuItem4.setFont(new java.awt.Font("Ebrima", 0, 11)); // NOI18N
		jMenuItem6.setFont(new java.awt.Font("Ebrima", 0, 11)); // NOI18N
		jMenuItem7.setFont(new java.awt.Font("Ebrima", 0, 11)); // NOI18N
		jMenuItem1.setFont(new java.awt.Font("Ebrima", 0, 11)); // NOI18N
		jMenuItem5.setFont(new java.awt.Font("Ebrima", 0, 11)); // NOI18N
		jMenuItem8.setFont(new java.awt.Font("Ebrima", 0, 11)); // NOI18N
		jMenuBar1.setFont(new java.awt.Font("Ebrima", 0, 12)); // NOI18N
		jMenu2.setFont(new java.awt.Font("Ebrima", 0, 12)); // NOI18N

		jMenuItem3.setFont(new java.awt.Font("Ebrima", 0, 11)); // NOI18N
		jMenuItem3.setText("Ayuda");
		jMenuItem8.setText("Tutorial");
		jMenuItem3.setToolTipText("");
		jMenu2.add(jMenuItem3);

		jMenuItem2.setText("Salir");
		jMenuItem4.setText("Wiki");

		jMenuItem6.setText("Preferences");
		jMenu1.add(jMenuItem4);
		jMenu1.add(jMenuItem2);
		jMenu1.setText("Open");
		jMenu2.setText("Help");

		jMenu4.setText("Window");
		jMenu3.setFont(new java.awt.Font("Ebrima", 0, 12)); // NOI18N
		jMenu4.setFont(new java.awt.Font("Ebrima", 0, 12)); // NOI18N

		jMenuBar1.add(jMenu1);
		jMenuBar1.add(jMenu4);
		jMenuBar1.add(jMenu3);

		jMenu2.add(jMenuItem8);
		jMenu2.add(jMenuItem1);
		jMenu4.add(jMenuItem6);
		
		jMenuBar1.add(jMenu2);
		setJMenuBar(jMenuBar1);

		// action listener acerca_de

		jMenuItem1.setText("About");
		jMenuItem1.addActionListener(new java.awt.event.ActionListener() {

			public void actionPerformed(java.awt.event.ActionEvent evt) {

				jMenuItem1ActionPerformed(evt);

				new About().setVisible(true);

			}

		});

		jMenuItem2.addActionListener(new java.awt.event.ActionListener() { // para salir de la aplicacion

			public void actionPerformed(java.awt.event.ActionEvent evt) {

				jMenuItem1ActionPerformed(evt);

				System.exit(1);

			}

		});

		jMenuItem3.addActionListener(new java.awt.event.ActionListener() { // para salir de la aplicacion

			public void actionPerformed(java.awt.event.ActionEvent evt) {

				jMenuItem1ActionPerformed(evt);

				try {

					new Ayuda().setVisible(true);

				} catch (Exception e) {

					e.printStackTrace();

				}

			}

		});

		jMenuItem4.addActionListener(new java.awt.event.ActionListener() { // para salir de la aplicacion

			public void actionPerformed(java.awt.event.ActionEvent evt) {

				jMenuItem1ActionPerformed(evt);

				String urlString = "https://gitlab.com/DavidEscamilla/HashCrack";
				openWebpage(urlString);

			}

		});

		jMenuItem5.addActionListener(new java.awt.event.ActionListener() { // para salir de la aplicacion

			public void actionPerformed(java.awt.event.ActionEvent evt) {

				jMenuItem1ActionPerformed(evt);

				new Harvester(usuario, pass, server).setVisible(true);

			}

		});

		jMenuItem6.addActionListener(new java.awt.event.ActionListener() {// preferencias

			public void actionPerformed(java.awt.event.ActionEvent evt) {

				jMenuItem1ActionPerformed(evt);

				new Preferences().setVisible(true);

			}

		});

		jMenuItem7.addActionListener(new java.awt.event.ActionListener() { // propiedades

			public void actionPerformed(java.awt.event.ActionEvent evt) {

				jMenuItem1ActionPerformed(evt);

			}

		});
		
		jMenuItem8.addActionListener(new java.awt.event.ActionListener() { // para salir de la aplicacion

			public void actionPerformed(java.awt.event.ActionEvent evt) {

				jMenuItem1ActionPerformed(evt);

				String urlString = "https://dagares.pythonanywhere.com/tutorial/";
				openWebpage(urlString);

			}

		});

		jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(
				new String[] { "Upload", "Convert", "List", "PotFile", "View", "Crack!" }));
		jComboBox1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {

				jComboBox1ActionPerformed(evt);
				@SuppressWarnings("rawtypes")
				JComboBox comboBox = (JComboBox) evt.getSource();

				Object selected = comboBox.getSelectedItem();

				if (selected.toString().equals("List")) {

					jTextField2.setVisible(false); // hash
					jTextField3.setVisible(false); // name
					jLabel1.setForeground(new java.awt.Color(219, 219, 219)); // name
					jLabel2.setForeground(new java.awt.Color(219, 219, 219)); // hash

				} else if (selected.toString().equals("View")) {

					jTextField2.setVisible(false);
					jLabel2.setForeground(new java.awt.Color(219, 219, 219)); // hash
					jTextField3.setVisible(true);
					jLabel1.setForeground(new java.awt.Color(0, 0, 0)); // name

				} else if (selected.toString().equals("Upload")) {

					jTextField2.setVisible(true);
					jTextField3.setVisible(true);
					jLabel1.setForeground(new java.awt.Color(0, 0, 0)); // name
					jLabel2.setForeground(new java.awt.Color(0, 0, 0)); // hash
					jLabel2.setText(" h4$h:");

				} else if (selected.toString().equals("Crack!")) {

					jTextField2.setVisible(false);
					//jLabel2.setText("Path:");
					jLabel2.setForeground(new java.awt.Color(219, 219, 219)); // hash
					jTextField3.setVisible(true);
					
					jLabel1.setForeground(new java.awt.Color(0, 0, 0)); // name

				} else if (selected.toString().equals("Convert")) {

					jTextField2.setVisible(true);
					jTextField3.setVisible(true);
					jLabel2.setText("Word:");
					jLabel1.setForeground(new java.awt.Color(0, 0, 0)); // name
					jLabel2.setForeground(new java.awt.Color(0, 0, 0)); // hash

				} else if (selected.toString().equals("PotFile")) {

					jTextField2.setVisible(false);
					jLabel2.setForeground(new java.awt.Color(219, 219, 219)); // hash
					jTextField3.setVisible(true);
					jLabel1.setForeground(new java.awt.Color(0, 0, 0)); // name
				}
			}
		});

		jComboBox2.setFont(new java.awt.Font("Ebrima", 0, 12));

		jComboBox2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "SHA256", "SHA1", "MD5" }));
		jComboBox2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jComboBox2ActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 751, Short.MAX_VALUE)
                        .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane1)
                            .addComponent(jSeparator1)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField2)
                                .addGap(10, 10, 10)
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jLabel1)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
	}// </editor-fold>

	private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
	}

	private void jTextField2ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
	}

	private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
	}

	private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
	}

	private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {

	}

	private void jComboBox2ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
	}

	public static void main(String args[]) {

		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}

		} catch (ClassNotFoundException ex) {

			java.util.logging.Logger.getLogger(Interfaz.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);

		} catch (InstantiationException ex) {

			java.util.logging.Logger.getLogger(Interfaz.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);

		} catch (IllegalAccessException ex) {

			java.util.logging.Logger.getLogger(Interfaz.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);

		} catch (javax.swing.UnsupportedLookAndFeelException ex) {

			java.util.logging.Logger.getLogger(Interfaz.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);

		}

		java.awt.EventQueue.invokeLater(new Runnable() {

			public void run() {

				new Interfaz(null, null, null).setVisible(true);

			}
		});
	}

	// Variables declaration - do not modify

	private javax.swing.JButton jButton1;
	private javax.swing.JButton jButton2;
	private javax.swing.JComboBox jComboBox1;
	private javax.swing.JComboBox jComboBox2;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JLabel jLabel5;
	private javax.swing.JMenu jMenu1;
	private javax.swing.JMenu jMenu2;
	private javax.swing.JMenu jMenu3;
	private javax.swing.JMenu jMenu4;
	private javax.swing.JMenuBar jMenuBar1;
	private javax.swing.JMenuItem jMenuItem1;
	private javax.swing.JMenuItem jMenuItem2;
	private javax.swing.JMenuItem jMenuItem3;
	private javax.swing.JMenuItem jMenuItem4;
	private javax.swing.JMenuItem jMenuItem5;
	private javax.swing.JMenuItem jMenuItem6;
	private javax.swing.JMenuItem jMenuItem7;
	private javax.swing.JMenuItem jMenuItem8;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JSeparator jSeparator1;
	public static javax.swing.JTextArea jTextArea1;
	private javax.swing.JTextField jTextField2;
	private javax.swing.JTextField jTextField1;
	private javax.swing.JTextField jTextField3;
	private javax.swing.JOptionPane jOptionPane1;

	// End of variables declaration
}
