/*  This file is part of HASHCRACK.

Foobar is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.


Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <https://www.gnu.org/licenses/>.

*/

package GUI;

import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import Clases.Ping_Connection;

public class Login extends javax.swing.JFrame {

	public Login() { //método para iniciar el frame

		initComponents();

	}

//obtenemos el icono para setearlo en nuestra aplicación    

	@Override
	public Image getIconImage() {

		Image retValue = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("resources/icon.png"));

		return retValue;
	}
	
	@SuppressWarnings("static-access")
	public void Log_in(String valorUser, String valorPass, String hostServer) {
		
		JOptionPane joption = new javax.swing.JOptionPane();
		Ping_Connection ping = new Ping_Connection();
		
		boolean result = ping.ping(valorUser, valorPass, hostServer, 22);
		
		if (result == false){
			
			joption.showMessageDialog(rootPane, "Hay algún problema con sus credenciales, revíselas.", "Credenciales erróneas", 0);
			
		} else {
			
			dispose();
			new Menu(valorUser,valorPass,hostServer).setVisible(true);
			joption.setFont(new java.awt.Font("Ebrima", 0, 12)); // NOI18N
			setFont(new java.awt.Font("Ebrima", 0, 12));
			joption.showMessageDialog(rootPane, " Inicio de sesión correcto", "Bienvenido", 1);
			
			}
		
	}

	@SuppressWarnings("unchecked")
	private void initComponents() {

		JOptionPane joption = new javax.swing.JOptionPane();
		
		jButton1 = new javax.swing.JButton();
		
		jLabel1 = new javax.swing.JLabel();
		jLabel2 = new javax.swing.JLabel();
		jLabel3 = new javax.swing.JLabel();
		jLabel4 = new javax.swing.JLabel();
		jLabel5 = new javax.swing.JLabel();
		jLabel6 = new javax.swing.JLabel();
		jSeparator1 = new javax.swing.JSeparator();
		
		jPasswordField1 = new javax.swing.JPasswordField();
		jTextField1 = new javax.swing.JTextField();
		jTextField2 = new javax.swing.JTextField();

		setBackground(new java.awt.Color(55, 195, 88));
		setTitle("Login ");
		setIconImage(getIconImage());
		setFont(new java.awt.Font("Ebrima", 0, 12));
		setResizable(false);

	// button ENTER

		jButton1.setFont(new java.awt.Font("Ebrima", 0, 12)); // NOI18N
		jButton1.setText("ENTER");
		
		jLabel3.setIcon(new ImageIcon(Login.class.getResource("/resources/monkey.png")));
		
		jTextField1.setBackground(new java.awt.Color(204, 204, 204));
		jTextField2.setBackground(new java.awt.Color(204, 204, 204));
        jTextField1.setFont(new java.awt.Font("Ebrima", 0, 12)); // NOI18N
        jTextField2.setFont(new java.awt.Font("Ebrima", 0, 12)); // NOI18N
        jPasswordField1.setBackground(new java.awt.Color(204, 204, 204));

		jButton1.addActionListener(new java.awt.event.ActionListener() {
			@SuppressWarnings("static-access")
			public void actionPerformed(java.awt.event.ActionEvent evt) {

			String valorPass = new String(jPasswordField1.getPassword());//contrase�a
			String valorUser = new String (jTextField1.getText());
			String hostServer = new String(jTextField2.getText());//server
			
			if (valorUser.equals("") && valorPass.equals("")) {
				
				joption.showMessageDialog(rootPane, "Introduzca usuario y/o password", "Advertencia", 1);
				
			}else if (valorUser.equals("")) {
				
				joption.showMessageDialog(rootPane, "Introduzca usuario", "Advertencia", 1);
				
			}else if (valorPass.equals("")) {
				
				joption.showMessageDialog(rootPane, "Introduzca password", "Advertencia", 1);
				
			} else {
				
				jButton1ActionPerformed(evt);
				Log_in(valorUser, valorPass, hostServer);
				
				}
			
			}

		}); // fin

		jLabel1.setFont(new java.awt.Font("Ebrima", 0, 14)); 
		jLabel1.setText("User:");

		jLabel2.setFont(new java.awt.Font("Ebrima", 0, 14)); 
		jLabel2.setText("Password:");
		
		jLabel4.setFont(new java.awt.Font("Ebrima", 0, 12));
        jLabel4.setText("GPLv3");

        jLabel5.setFont(new java.awt.Font("Ebrima", 0, 12));
        jLabel5.setText("beta_1.0");
        
        jLabel6.setFont(new java.awt.Font("Ebrima", 0, 14));
        jLabel6.setText("Server:");

		jTextField1.addActionListener(new java.awt.event.ActionListener() {

			@SuppressWarnings("static-access")
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				
				String valorPass = new String(jPasswordField1.getPassword());//contrase�a
				String valorUser = new String (jTextField1.getText());//usuario
				String hostServer = new String(jTextField2.getText());//server
				
				if (valorUser.equals("") && valorPass.equals("")) {
					
					joption.showMessageDialog(rootPane, "Introduzca usuario y/o password", "Advertencia", 1);
					
				}else if (valorUser.equals("")) {
					
					joption.showMessageDialog(rootPane, "Introduzca usuario", "Advertencia", 1);
					
				}else if (valorPass.equals("")) {
					
					joption.showMessageDialog(rootPane, "Introduzca password", "Advertencia", 1);
					
				} else {
					
					jButton1ActionPerformed(evt);
					Log_in(valorUser, valorPass, hostServer);
					
					}
			}

		});
		
		jPasswordField1.addActionListener(new java.awt.event.ActionListener() {
			@SuppressWarnings("static-access")
			public void actionPerformed(java.awt.event.ActionEvent evt) {

				String valorPass = new String(jPasswordField1.getPassword());//contrase�a
				String valorUser = new String (jTextField1.getText());
				String hostServer = new String(jTextField2.getText());//server
				
				if (valorUser.equals("") && valorPass.equals("")) {
					
					joption.showMessageDialog(rootPane, "Introduzca usuario y/o password", "Advertencia", 1);
					
				} else if (valorUser.equals("")) {
					
					joption.showMessageDialog(rootPane, "Introduzca usuario", "Advertencia", 1);
					
				} else if (valorPass.equals("")) {
					
					joption.showMessageDialog(rootPane, "Introduzca password", "Advertencia", 1);	
					
				} else {
					
					jButton1ActionPerformed(evt);
					Log_in(valorUser, valorPass, hostServer);
					
					}
			}

		});
		
		jTextField2.addActionListener(new java.awt.event.ActionListener() {

			@SuppressWarnings("static-access")
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				
				String valorPass = new String(jPasswordField1.getPassword());//contrase�a
				String valorUser = new String (jTextField1.getText());//usuario
				String hostServer = new String(jTextField2.getText());//server
				
				if (valorUser.equals("") && valorPass.equals("")) {
					
					joption.showMessageDialog(rootPane, "Introduzca usuario y/o password", "Advertencia", 1);
					
				}else if (valorUser.equals("")) {
					
					joption.showMessageDialog(rootPane, "Introduzca usuario", "Advertencia", 1);
					
				}else if (valorPass.equals("")) {
					
					joption.showMessageDialog(rootPane, "Introduzca password", "Advertencia", 1);
					
				} else {
					
					jButton1ActionPerformed(evt);
					Log_in(valorUser, valorPass, hostServer);
					
					}
			}

		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel1)
                                    .addComponent(jLabel6))
                                .addGap(100, 100, 100)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jPasswordField1, javax.swing.GroupLayout.DEFAULT_SIZE, 171, Short.MAX_VALUE)
                                    .addComponent(jTextField1)
                                    .addComponent(jTextField2, javax.swing.GroupLayout.DEFAULT_SIZE, 171, Short.MAX_VALUE))
                                .addGap(46, 46, 46))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(124, 124, 124))))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 488, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel4)))
                        .addContainerGap())))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addGap(215, 215, 215))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 141, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jPasswordField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)))
        );

        pack();                    
		setLocationRelativeTo(null);

	}

	private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
		
	}

	public static void main(String args[]) {

		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}

		java.awt.EventQueue.invokeLater(new Runnable() {
			
			public void run() {
				
				new Login().setVisible(true);
			}
			
		});
		
	}

	private javax.swing.JButton jButton1;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JLabel jLabel5;
	private javax.swing.JLabel jLabel6;
	private javax.swing.JSeparator jSeparator1;
	private javax.swing.JPasswordField jPasswordField1;
	private javax.swing.JTextField jTextField1;
	private javax.swing.JTextField jTextField2;

// End of variables declaration                   
}