/*  This file is part of HASHCRACK.

Foobar is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.


Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <https://www.gnu.org/licenses/>.

*/

package Utils;
 
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
 
/*
 
 -- Clase encargada de establecer conexion y ejecutar comandos SSH.
 
 */

public class SSHConnector {
 
    private final String ENTER_KEY = "\n"; // para el espacio entre lineas
    
//Sesion SSH establecida.
    
    private Session session;
 
    //Establece una conexion SSH.
    
    public void connect(String username, String password, String host, int port)
    
        throws JSchException, IllegalAccessException {
    	
        if (this.session == null || !this.session.isConnected()) {
        	
            JSch jsch = new JSch();
 
            this.session = jsch.getSession(username, host, port);
            this.session.setPassword(password);
 
       // Parametro para no validar la key de conexion.
            
            this.session.setConfig("StrictHostKeyChecking","no");
            
            this.session.connect();
            
        } else {
        	
            throw new IllegalAccessException("Sesion SSH ya iniciada.");
        }
    }
    
    public final String executeCommand(String command)
    
        throws IllegalAccessException, JSchException, IOException {
    	
        if (this.session != null && this.session.isConnected()) {
 
           // Abrimos un canal SSH. Es como abrir una consola.
        	
            ChannelExec channelExec = (ChannelExec) this.session.openChannel("exec");
        
            InputStream in = channelExec.getInputStream();
 
           // Ejecutamos el comando.
            
            channelExec.setCommand(command);
            channelExec.connect();
 
           // Obtenemos el texto impreso en la consola.
            
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            StringBuilder builder = new StringBuilder();
            String linea;
 
            while ((linea = br.readLine()) != null) {
            	
                builder.append(linea);
                builder.append(ENTER_KEY); // "\n para que salga linea por linea 
                
            }
            
            channelExec.disconnect();
 
       //Recibimos y mostramos la informacion del comando
            
            return builder.toString();
            
        } else {
        	
            throw new IllegalAccessException("No existe sesion SSH iniciada.");
        }
    }
 
    
//Cierra la sesi�n SSH.
     
    
    public final void disconnect() {
    	
        this.session.disconnect();
        
    }
}