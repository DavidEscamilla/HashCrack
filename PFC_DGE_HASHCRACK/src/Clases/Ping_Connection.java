/*  This file is part of HASHCRACK.

Foobar is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.


Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <https://www.gnu.org/licenses/>.

*/

package Clases;

import java.io.IOException;
import java.net.UnknownHostException;

import com.jcraft.jsch.JSchException;

import Utils.SSHConnector;

public class Ping_Connection {

	public boolean ping(String valorUser, String valorPass, String hostServer, int port) {

		SSHConnector sshConnector = new SSHConnector();

		try {

			sshConnector.connect(valorUser, valorPass, hostServer, port);
			sshConnector.executeCommand("ls");
			sshConnector.disconnect();
			return true;

		} catch (IllegalAccessException e) {
			
			sshConnector.disconnect();
			e.printStackTrace();
			return false;

		} catch (JSchException e) {

			e.printStackTrace();
			return false;

		} catch (UnknownHostException e) {

			e.printStackTrace();
			return false;

		} catch (IOException e) {

			e.printStackTrace();
			return false;
		}

	}
}
