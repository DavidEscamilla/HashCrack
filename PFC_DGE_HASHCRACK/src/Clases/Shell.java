/*  This file is part of HASHCRACK.

	Foobar is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.


	Foobar is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Foobar.  If not, see <https://www.gnu.org/licenses/>.

*/

package Clases;

import Utils.SSHConnector;
import com.jcraft.jsch.JSchException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

public class Shell {

	public final int HOST = 22;
	public final String urlDic = "/usr/share/wordlists/list.txt";

//metodo para realizar la ejecucion de los comandos
//necesarios para realizar las funciones principales de la aplicacion

	public String Consola(String opcion, String comando, String nombre, String extension, String user, String password,
			String server) {

		String result = "a";

		try {

			SSHConnector sshConnector = new SSHConnector();
			sshConnector.connect(user, password, server, HOST);

			// ----->> condicionales
			// primer condicional para elegir list

			if (opcion.equals("List")) {

				if (extension.equals("SHA256")) {

					if (result.equals("")) {

						return "" + "	User: " + user + "Todavía no existe ningún hash sha256";

					} else {

						result = sshConnector.executeCommand("\nls sha256_*"); // listar hashes
						sshConnector.disconnect();

						return "------------list of hashes sha256-------------->\n" + "\n" + result.toString()
								+ "\nend of list ----->";

					}

				} else if (extension.equals("MD5")) {

					if (result.equals("")) {

						return "" + "	User: " + user + "Todavía no existe ningún hash md5";

					} else {

						result = sshConnector.executeCommand("\nls md5_*"); // listar hashes
						sshConnector.disconnect();

						return "------------list of hashes md5-------------->\n" + "\n" + result.toString()
								+ "\nend of list ----->";

					}

				} else if (extension.equals("SHA1")) {

					if (result.equals("")) {

						return "" + "	User: " + user + "Todavía no existe ningún hash sha1";

					} else {

						result = sshConnector.executeCommand("\nls sha1_*"); // listar hashes
						sshConnector.disconnect();

						return "------------list of hashes sha1-------------->\n" + "\n" + result.toString()
								+ "\nend of list ----->";
					}
					
				} else {

					sshConnector.disconnect();
					return "CRITICAL ERROR";

				} // fin del concional para listar los tipos de hashes alamcenados

				// condicional para elegir convertir de texto-plano a md5

			} else if (opcion.equals("Convert")) {

				result = sshConnector.executeCommand("\nls sha256_" + nombre);// listar hashes

				if (extension.equals("SHA256")) {

					if (result.contains("sha256_" + nombre)) {

						return "Error : \n"

								+ "Ya existe un archivo llamado sha256_" + nombre + "" + "" + "\n";

					} else {

						sshConnector.executeCommand("echo " + comando + "  >> " + urlDic);
						result = sshConnector.executeCommand(
								"echo -n \"" + comando + "\" | sha256sum | tr -d \" -\" >> sha256_" + nombre); // conversor
																												// de
																												// texto-plano
																												// a
																												// SHA256
						sshConnector.disconnect();

						return "\n" + result.toString() + "sha256_" + nombre
								+ " transformado a sha256sum y almacenado correctamente.";

					}

				} else if (extension.equals("MD5")) {

					result = sshConnector.executeCommand("\nls md5_" + nombre);// listar hashes

					if (result.contains("md5_" + nombre)) {

						return "Error : \n"

								+ "Ya existe un archivo llamado md5_" + nombre + "" + "" + "\n";

					} else {

						sshConnector.executeCommand("echo " + comando + "  >> " + urlDic);

						result = sshConnector
								.executeCommand("echo -n \"" + comando + "\" | md5sum | tr -d \" -\" >> md5_" + nombre);
						sshConnector.disconnect();

						return "\n" + result.toString() + "md5_" + nombre
								+ " transformado a md5sum y almacenado correctamente.";

					}

				} else if (extension.equals("SHA1")) {

					result = sshConnector.executeCommand("\nls sha1_" + nombre);// listar hashes

					if (result.contains("sha1_" + nombre)) {

						return "Error : \n"

								+ "Ya existe un archivo llamado sha1_" + nombre + "" + "" + "\n";

					} else {

						sshConnector.executeCommand("echo " + comando + "  >> " + urlDic);

						result = sshConnector.executeCommand(
								"echo -n \"" + comando + "\" | sha1sum | tr -d \" -\" >> sha1_" + nombre); // conversor
																											// de texto
																											// plano a
																											// MD5
						sshConnector.disconnect();

						return "\n" + result.toString() + "sha1_" + nombre
								+ " transformado a sha1sum y almacenado correctamente.";

					}

				} else {

					sshConnector.disconnect();
					return "CRITICAL ERROR";

				}

				// condicional para crackear la password

			} else if (opcion.equals("Crack!")) {

				if (extension.equals("SHA256")) {

					result = sshConnector
							.executeCommand("hashcat -m 1400 " + "sha256_" + nombre + " " + urlDic + " --force"); // crackear
																													// path
																													// Diccionario
					sshConnector.disconnect();

					return "\n" + result.toString();

				} else if (extension.equals("MD5")) {

					result = sshConnector.executeCommand("hashcat -m 0 " + "md5_" + nombre + " " + urlDic + " --force"); // crackear
					sshConnector.disconnect();

					return "\n" + result.toString();

				} else if (extension.equals("SHA1")) {

					result = sshConnector
							.executeCommand("hashcat -m 100 " + "sha1_" + nombre + " " + urlDic + " --force"); // crackear
					sshConnector.disconnect();

					return "\n" + result.toString();

				} else {

					sshConnector.disconnect();
					return "CRITICAL ERROR";

				}

				// condicional para usar harvester

			} else if (opcion.equals("Harvester")) { // the harvester

				result = sshConnector.executeCommand("theharvester -d " + comando + " -b " + nombre); // the harvester
																										// para indexar
																										// correos
				sshConnector.disconnect();

				return "\n" + result.toString();

				// potFile condicionales --------------------------------->>

			} else if (opcion.equals("PotFile")) {

				if (extension.equals("SHA256")) {

					result = sshConnector
							.executeCommand("\nhashcat -m 1400 " + "sha256_" + nombre + " " + urlDic + " --show \n");

					if (result.contains("Token length exception")) { // fallo por pasarle hash inexistente

						sshConnector.disconnect();
						return "No existe ningun hash SHA256 con ese nombre: " + nombre;

					} else if (result.equals("")) { // fallo por no tener almacenado

						sshConnector.disconnect();
						return "Debe de crackear el hash sha256_" + nombre;

					} else {

						sshConnector.disconnect();
						return "\nSHA256: " + result.toString();

					}

				} else if (extension.equals("MD5")) {

					result = sshConnector
							.executeCommand("\nhashcat -m 0 " + "md5_" + nombre + " " + urlDic + " --show \n");

					if (result.contains("Token length exception")) { // fallo por pasarle hash inexistente

						sshConnector.disconnect();
						return "No existe ningun hash MD5 con ese nombre: " + nombre;

					} else if (result.equals("")) { // fallo por no tener almacenado

						sshConnector.disconnect();
						return "Debe de crackear el hash md5_" + nombre;

					} else {

						sshConnector.disconnect();
						return "\nMD5: " + result.toString();

					}

				} else if (extension.equals("SHA1")) {

					result = sshConnector
							.executeCommand("\nhashcat -m 100 " + "sha1_" + nombre + " " + urlDic + " --show \n");

					if (result.contains("Token length exception")) { // fallo por pasarle hash inexistente

						sshConnector.disconnect();
						return "No existe ningun hash SHA1 con ese nombre: " + nombre;

					} else if (result.equals("")) { // fallo por no tener almacenado en potFile

						sshConnector.disconnect();
						return "Debe de crackear el hash sha1_" + nombre;

					} else {

						sshConnector.disconnect();
						return "\nSHA1: " + result.toString();

					}

				} else {

					sshConnector.disconnect();
					return "CRITICAL ERROR";

				}

				// condicional para visualizar

			} else if (opcion.equals("View")) {

				// SHA256
				if (extension.equals("SHA256")) {

					result = sshConnector.executeCommand("\n cat " + "sha256_" + nombre);

					if (result.equals("")) {

						sshConnector.disconnect();
						return "No se ha encontrado ningún hash con ese nombre";

					} else {

						sshConnector.disconnect();
						return "\nFile: sha256_" + nombre + "\n" + "\n---> SHA256: " + result.toString();
					}

					// MD5
				} else if (extension.equals("MD5")) {

					result = sshConnector.executeCommand("\n cat " + "md5_" + nombre);

					if (result.equals("")) {

						sshConnector.disconnect();
						return "No se ha encontrado ningún hash con ese nombre";

					} else {

						sshConnector.disconnect();
						return "\nFile: md5_" + nombre + "\n" + "\n---> MD5: " + result.toString();
					}

					// SHA1
				} else if (extension.equals("SHA1")) {

					result = sshConnector.executeCommand("\n cat " + "sha1_" + nombre);

					if (result.equals("")) {

						sshConnector.disconnect();
						return "No se ha encontrado ningún hash con ese nombre";

					} else {

						sshConnector.disconnect();

						return "\nFile: sha1_" + nombre + "\n" + "\n---> SHA1: " + result.toString();
					}

				} else {

					sshConnector.disconnect();
					return "CRITICAL ERROR";

				}

			} else if (opcion.equals("Upload")) {

				Sftp_shell sftp = new Sftp_shell();
				return sftp.Sftp(comando, nombre, extension, user, password, server);
			}

		} catch (JSchException ex) {

			ex.printStackTrace();
			return "Error en la autenticación, revise sus credenciales\n" + "Para más info:_"
					+ "\nPongase en contacto con el administrador de la aplicación."
					+ "\ncontact: dagares@campusaula.com";

		} catch (IllegalAccessException ex) {

			ex.printStackTrace();
			return " No se ha podido establecer conexión con la base de datos"
					+ "\nPongase en contacto con el administrador de la aplicación." + "\n";

		} catch (IOException ex) {

			ex.printStackTrace();
			return "\nHASHCRACK:\n" + "Excepcion: " + ex.getMessage();

		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "CRITICAL ERROR";

	}
}