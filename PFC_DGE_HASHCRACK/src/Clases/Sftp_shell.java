/*  This file is part of HASHCRACK.

Foobar is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.


Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <https://www.gnu.org/licenses/>.

*/

package Clases;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class Sftp_shell {
	
	public final int HOST = 22;
	
	
	public String Sftp(String hash, String nombre, String extension, String usuario, String password, String server)
			throws FileNotFoundException, UnsupportedEncodingException, NoSuchAlgorithmException, JSchException {

		JSch jsch = new JSch(); // creamos el objeto JSch
		Session session = jsch.getSession(usuario, server, HOST);
		session.setPassword(password);
		
		if (extension.equals("SHA256")) { // cuando se sube el archivo con mismo nombre mata el archivo original y
											// sobreescribe por el nuestro

			try {

				File f1 = new File("sha256_" + nombre); // creamos archivo
				PrintWriter writer = new PrintWriter("sha256_" + nombre, "UTF-8"); // seteamos el writer
				writer.println(hash.toString()); // escribimos el hash

				writer.close(); // cierre de writer

				session.setConfig("StrictHostKeyChecking", "no");
				System.out.println("----------sha256------------");
				System.out.println("Establishing Connection...");
				session.connect();

				System.out.println("Connection established.");
				System.out.println("Creating SFTP Channel.");

				ChannelSftp sftpChannel = (ChannelSftp) session.openChannel("sftp");
				sftpChannel.connect();
				sftpChannel.put(new FileInputStream(f1), f1.getName(), ChannelSftp.OVERWRITE);

				f1.deleteOnExit();

				// cerramos sesion

				sftpChannel.disconnect();
				session.disconnect();

			} catch (JSchException | SftpException e) {

				e.printStackTrace();
			}
			return  "\nArchivo sha256_"+ nombre +" subido correctamente";

		} else if (extension.equals("MD5")) {

			try {

				File f1 = new File("md5_" + nombre); // creamos archivo
				PrintWriter writer = new PrintWriter("md5_" + nombre, "UTF-8"); // seteamos el writer
				writer.println(hash.toString()); // escribimos el hash

				writer.close(); // cierre de writer

				session.setConfig("StrictHostKeyChecking", "no");
				System.out.println("-------------------------");
				System.out.println("Establishing Connection...");
				session.connect();

				System.out.println("Connection established.");
				System.out.println("Creating SFTP Channel.");

				ChannelSftp sftpChannel = (ChannelSftp) session.openChannel("sftp");
				sftpChannel.connect();
				sftpChannel.put(new FileInputStream(f1), f1.getName(), ChannelSftp.OVERWRITE);

				f1.deleteOnExit();

				// cerramos sesion

				sftpChannel.disconnect();
				session.disconnect();

			} catch (JSchException | SftpException e) {

				e.printStackTrace();
			}

			return  "\nArchivo md5_"+ nombre +" subido correctamente";
			
		} else if (extension.equals("SHA1")) {

				try {

					File f1 = new File("sha1_" + nombre); // creamos archivo
					PrintWriter writer = new PrintWriter("sha1_" + nombre, "UTF-8"); // seteamos el writer
					writer.println(hash.toString()); // escribimos el hash

					writer.close(); // cierre de writer

					session.setConfig("StrictHostKeyChecking", "no");
					System.out.println("-------------------------");
					System.out.println("Establishing Connection...");
					session.connect();

					System.out.println("Connection established.");
					System.out.println("Creating SFTP Channel.");

					ChannelSftp sftpChannel = (ChannelSftp) session.openChannel("sftp");
					sftpChannel.connect();
					sftpChannel.put(new FileInputStream(f1), f1.getName(), ChannelSftp.OVERWRITE);

					f1.deleteOnExit();

					// cerramos sesion

					sftpChannel.disconnect();
					session.disconnect();

				} catch (JSchException | SftpException e) {

					e.printStackTrace();
				}

				return  "\nArchivo sha1_"+ nombre +" subido correctamente";		
		
		} else {
			
			session.disconnect();
			return "Hay algún tipo de error con la conexión";
		}

	}

}
