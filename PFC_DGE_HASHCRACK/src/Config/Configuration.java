/*  This file is part of HASHCRACK.

Foobar is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.


Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <https://www.gnu.org/licenses/>.

*/

package Config;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.URI;

import GUI.Interfaz;
import Config.*;

public class Configuration {

	public String COLORB = "";

	public void checkColor() throws IOException { //metodo para comprobar que color está escrito en el config.txt

		File f = new File("config.txt");

		if (f.exists()) {

			try {

				FileReader fr = new FileReader(f);
				BufferedReader br = new BufferedReader(fr);
				StringBuilder sb = new StringBuilder();
				String linea;

				while ((linea = br.readLine()) != null) {

					sb.append(linea);

				}

				
				COLORB = sb.toString();

				br.close();
				fr.close();

				if (COLORB.equals("black")) {

					Interfaz.jTextArea1.setBackground(new java.awt.Color(0, 0, 0));
					Interfaz.jTextArea1.setForeground(new java.awt.Color(55, 195, 88));

				} else if (COLORB.equals("white")) {

					Interfaz.jTextArea1.setForeground(new java.awt.Color(0, 0, 0));
					Interfaz.jTextArea1.setBackground(new java.awt.Color(255, 255, 255));

				}

			} catch (Exception e) {

				System.out.println(e.getMessage());
			}

		} else {
			
			Interfaz.jTextArea1.setForeground(new java.awt.Color(0, 0, 0));
			Interfaz.jTextArea1.setBackground(new java.awt.Color(255, 255, 255));

			PrintWriter writer = new PrintWriter("config.txt", "UTF-8");// seteamos el writer
			writer.println("white"); // escribimos el hash

			writer.close();
		}

	}

	public void backColor() throws IOException { //metodo para seleccionar el color

		try {

			File f = new File("config.txt");
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			StringBuilder sb = new StringBuilder();
			String linea;

			while ((linea = br.readLine()) != null) {

				sb.append(linea);

			}

			COLORB = sb.toString();
			br.close();

			if (COLORB.equals("white")) {

				Interfaz.jTextArea1.setBackground(new java.awt.Color(0, 0, 0));
				Interfaz.jTextArea1.setForeground(new java.awt.Color(55, 195, 88));

				PrintWriter writer = new PrintWriter("config.txt", "UTF-8"); // seteamos el writer
				writer.println("black"); // escribimos el hash

				writer.close();

			} else if (COLORB.equals("black")) {

				Interfaz.jTextArea1.setForeground(new java.awt.Color(0, 0, 0));
				Interfaz.jTextArea1.setBackground(new java.awt.Color(255, 255, 255));

				PrintWriter writer = new PrintWriter("config.txt", "UTF-8");// seteamos el writer
				writer.println("white"); // escribimos el hash

				writer.close();
			}

		} catch (Exception e) {

			System.out.println(e.getMessage());

		}

	}
	
}
