# HashCrack_beta ![version](https://img.shields.io/badge/version-1.0-green.svg) 
<!--![build](https://img.shields.io/continuousphp/git-hub/doctrine/dbal/master.svg)-->


Crackear cualquier contraseña encriptada y convertir texto plano en SHA256,SHA1,MD5 utilizando la potencia de hashcat y como funcionalidad extra he añadido [The Harvester](https://github.com/laramies/theHarvester) para indexar correos y subdominios.

## Introducción

HashCrack es un proyecto de Final de Grado superior de aplicaciones multiplataforma,
que utilizando hashcat + diccionarios realiza en muy poco tiempo el crackeo de un hash (SHA1-SHA256-MD5).

La aplicación está desarrollada en Java y la interfaz está diseñada con NetBeans, 
también hago uso de una dependencia maven para la conexión con el servidor tanto SSH como FTP.

### Pre-requisitos

```
Java mínimo version 1.4.0
```
```
Distribución de seguridad linux (ParrotSec o Kali-Linux)
```

### Ejecución de HashCrack

Elija su sistema operativo:


* [Windows_x86](https://mega.nz/#!a6YzAYqK!e5H2U6GmM8Gis9bZrwJZRgq90BKPlgKZbgnSXXJNE60)
* [Linux](https://mega.nz/#!u7BEiAxY!CnMpKl07TKso0vuD7BC7NlZsKQc2myNppsWu1gcPS0A)

## Tests de ejecución

La aplicación ha sido testeada en el IDE de Eclipse para Java.

## Desarrollo

* [Eclipse](https://www.eclipse.org/) - IDE de desarrollo del proyecto.
* [NetBeans](https://netbeans.org/) - Diseñar interfaces.
* [Maven](https://maven.apache.org/) - Dependency Management.
* [Jsch](https://mvnrepository.com/artifact/com.jcraft/jsch) - Librería para la conexión.


## Compilación


* [Eclipse](https://www.eclipse.org/) - Convertir el proyecto a .jar.
* [Launch4j](http://launch4j.sourceforge.net/) - Usado para convertir el .jar a .exe.

## Contributing

Leasé [CONTRIBUTING.md](https://gitlab.com/DavidEscamilla/HashCrack) para los detalles sobre el código de conducta, and the process for submitting pull requests to us.

## Control de versiones

He utilizado [Git](http://gitlab.com) para el control de versiones.Para ver los cambios, véase, [HashCrack](https://gitlab.com/DavidEscamilla/HashCrack).

## Autores

* **David Garcia** - *Master* - [HashCrack](https://gitlab.com/DavidEscamilla/HashCrack)
* **Christian Martorella** - *Owner* - [The Harvester3.0.6](https://github.com/laramies/theHarvester)
* **Jens Steube** - *Owner* - [HashCat](https://github.com/hashcat/hashcat)

See also the list of [contributors](https://gitlab.com/DavidEscamilla/HashCrack/blob/master/CONTRIBUTING.md) who participated in this project.

## Licencia

Este proyecto está desarrollado bajo la licencia - véase el [LICENSE](https://gitlab.com/DavidEscamilla/HashCrack/blob/master/LICENSE) archivo de licencia para más detalles.

## Motivaciones

* Desarrollar mi propia aplicación para la comunidad libre.
* 
* 
*
